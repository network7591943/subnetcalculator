import ipaddress

def subnet_calculator(ip_address, subnet_mask):
    network = ipaddress.ip_network(f"{ip_address}/{subnet_mask}", strict=False)
    broadcast = network.broadcast_address
    num_hosts = network.num_addresses - 2
    first_ip = network.network_address + 1
    last_ip = broadcast - 1

    print(f"Network Address: {network.network_address}")
    print(f"First Usable IP: {first_ip}")
    print(f"Last Usable IP: {last_ip}")
    print(f"Broadcast Address: {broadcast}")
    print(f"Number of Usable Hosts: {num_hosts}")

# เปลี่ยนค่า IP Address และ Subnet Mask ตามที่คุณต้องการ
ip_address = "172.16.0.1"
subnet_mask = "255.255.255.240"

subnet_calculator(ip_address, subnet_mask)